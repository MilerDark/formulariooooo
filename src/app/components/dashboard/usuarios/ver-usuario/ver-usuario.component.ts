import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-ver-usuario',
  templateUrl: './ver-usuario.component.html',
  styleUrls: ['./ver-usuario.component.css']
})
export class VerUsuarioComponent implements OnInit {
  form!: FormGroup;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private usuarioService: UsuarioService,
              private fb: FormBuilder) {

    this.activatedRoute.params.subscribe(params => {
      const id = params['id'];
      console.log(id);
      const usuario = this.usuarioService.buscarUsuario(id);
      console.log(usuario);

      //Verifico de la longitud del objeto es cero
      if(Object.keys(usuario).length === 0){
        this.router.navigate(['/dashboard/usuarios']);
      }
      this.form = this.fb.group({
        usuario: ['', Validators.required],
        nombre: ['', Validators.required],
        apellido: ['', Validators.required],
        sexo: ['', Validators.required]
       });
       //patchValue modifica uno o varios elementos
       this.form.patchValue({
        usuario: usuario.usuario,
        nombre: usuario.nombre,
        apellido: usuario.apellido,
        sexo: usuario.sexo
       });
      });
     }
        
  ngOnInit(): void {
  }

  Volver(): void {
    this.router.navigate(['/dashboard/usuarios']);
  }

}
